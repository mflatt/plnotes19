#lang scribble/manual
@(require (for-label racket/base
                     racket/system)
          "little.rkt")

@title[#:tag "macros"]{Macros}

To a first approximation, macros in Racket are like macros in Curly,
except that

@itemlist[

 @item{instead of @tt{let-macro} to bind a local macro,
       @racket[define-syntax] defines a macro for a module body.}

 @item{instead of an S-expression, a Racket macro receives a syntax
       object as its argument, and it must produce a syntax object as
       its results; and}

 @item{instead of having an unchangeable Curly language that is used
       to implement macros, the syntactic forms and primitives used to
       implement a Racket macro are determined by an explicit
       @racket[require] with @racket[for-syntax].}

]

@; ----------------------------------------
@section{Defining Macros}

The following module defines and uses the macro @racket[five], which
simply expands to @racket[5]:

@racketmod[
racket/base
(require (for-syntax racket/base))

(define-syntax five
  (lambda (stx)
    #'5))

(+ (five) (five anything-here x [y z] ((1 2)) "3"))
]

Running this program produces @racketresultfont{10}, because it simply
adds @racket[5] and @racket[5]. The transformer function for
@racket[five] takes a @racket[stx] argument, but ignores it, so that's
why @racket[(five)] and @racket[(five anything-here x [y z] ((1 2))
"3")]. If we want a @racket[five] that works only when uses as
@racket[(five)], then the transformer just needs to inspect
@racket[stx]:

@racketmod[
racket/base
(require (for-syntax racket/base))

(define-syntax five
  (lambda (stx)
    (if (= 1 (length (syntax->list stx)))
        #'5
        (raise-syntax-error #f "bad syntax" stx))))

(+ (five) #,(pink @racket[(five anything-here x [y z] ((1 2)) "3")]))
]

Passing @racket[stx] as the third argument to
@racket[raise-syntax-error] allows DrRacket to highlight the erroneous
expression in pink, since the syntax object carries its source
location to be recorded as part of the exception.

Instead of writing an explicit @racket[lambda], macros are often
written using a function shorthand built into @racket[define-syntax]
that is like the one built into @racket[define]:

@racketmod[
racket/base
(require (for-syntax racket/base))

(define-syntax (five stx)
  (if (= 1 (length (syntax->list stx)))
      #'5
      (raise-syntax-error #f "bad syntax" stx)))

(+ (five) (five))
]

The @racket[five] macro is not useful, but we can use just these tools
to build @racket[time-it]:

@racketmod[
racket/base
(require (for-syntax racket/base))

(define (time-thunk thunk)
  (let* ([before (current-inexact-milliseconds)]
         [answer (thunk)]
         [after (current-inexact-milliseconds)])
   (printf "It took ~a ms to compute.\n" (- after before))
   answer))

(define-syntax (time-it stx)
  (datum->syntax #'here
                 (list #'time-thunk
                       (list #'lambda
                             #'()
                             (list-ref (syntax->list stx) 1)))))

(time-it (+ 1 2))
]

More compactly, using @racket[quasisyntax]:

@RACKETBLOCK[
(define-syntax (time-it stx)
  #`(time-thunk (lambda () #,(list-ref (syntax->list stx) 1))))
]

With a check to make sure that @racket[time-it] is used correctly:

@RACKETBLOCK[
(define-syntax (time-it stx)
  (unless (= 2 (length (syntax->list stx)))
    (raise-syntax-error #f "bad syntax" stx))
  #`(time-thunk (lambda () #,(list-ref (syntax->list stx) 1))))
]

@; ----------------------------------------
@section{Run Time and Compile Time}

As another example, suppose that we want a lightweight way to run
external programs. Instead of using library functions like
@racket[system*] and @racket[find-executable-path] in

@racket[
(system* (find-executable-path "ls") "-l")
]

we'd like to write just

@racket[
(run ls -l)
]

To make this @racket[run] form work, the identifiers @racketidfont{ls}
and @racketidfont{-l} have to be treated as literal symbols and
converted into strings, instead of treating them as variable
references.

@racketmod[#:escape UNSYNTAX
racket/base
(require racket/system
         (for-syntax racket/base))

(define (run-program prog-str . arg-strs)
  (apply system* (find-program prog-str) arg-strs))
 
(define (find-program str)
  (or (find-executable-path str)
      (error 'pfsh "could not find program: ~a" str)))

(define-syntax (run stx)
  #`(run-program #,@(map (lambda (id)
                           #`(symbol->string '#,id))
                         (list-tail (syntax->list stx) 1))))

(run ls -l)
]

With this implementation of @racket[run], the expression

@racketblock[(run ls -l)]

expands to

@racketblock[(run-program (symbol->string 'run) (symbol->string 'ls) (symbol->string '-l))]

But we'd prefer to perform the conversion once and for all runs at
compile time, so that the expansion would be

@racketblock[(run-program "run" "ls" "-l")]

To do that, we need to make sure the @racket[symbol->string] call is
in a compile-time position instead of within generated run-time code:

@racketblock[
(define-syntax (run stx)
  #`(run-program #,@(map (lambda (id)
                           (symbol->string (syntax-e id)))
                         (list-tail (syntax->list stx) 1))))
]

@; ----------------------------------------
@section[#:tag "macro-exercises"]{Exercises}

@itemlist[
#:style 'ordered

@item{Implement the macro @racket[define-five], which is like
@racket[define], but the behavior of @racket[define-five] is specified
only when the right-hand expression has the value five:

@racketblock[
(define-five x (+ 4 1))
(+ x x) (code:comment "=> 10")
]

Your macro should implement the obvious optimization, but it should
also warn the programmer about the optimization's assumption at
compile time. For example, the output of

@racketblock[
(define-five x (+ 4 1))
(+ x x)

(define-five y (+ 3 3))
(+ y y)
]

should be

@racketblock[
@#,racketoutput{Assuming expression produces 5: (+ 4 1)}
@#,racketoutput{Assuming expression produces 5: (+ 3 3)}
10
10
]

Note that both printouts occur first, since they happen at compile
time, and the first @racketresultfont{10} prints only when run time
start.

You can print an S-expression @racket[_se] using

@racketblock[(printf "~s\n" _se)]}

@item{Change @racket[define-five] to print its warning at run time
instead of compile time. So, the output of

@racketblock[
(define-five x (+ 4 1))
(+ x x)

(define-five y (+ 3 3))
(+ y y)
]

should be

@racketblock[
@#,racketoutput{Assuming expression produces 5: (+ 4 1)}
10
@#,racketoutput{Assuming expression produces 5: (+ 3 3)}
10
]}

]
