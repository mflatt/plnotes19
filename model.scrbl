#lang scribble/manual
@(require scribble/bnf
          scribble/example
          "utils.rkt"
          (for-syntax racket/base
                      syntax/strip-context)
          (for-label (except-in (only-meta-in 0 plait) ....)))

@(define lambda+let.rkt
   @ext["lambda+let.rkt"]{@tt{lambda+let.rkt}})
@(define lambda.rkt
   @ext["lambda.rkt"]{@tt{lambda.rkt}})
@(define let-macro.rkt
   @ext["let-macro.rkt"]{@tt{let-macro.rkt}})
   
@(define open @litchar["{"])
@(define close @litchar["}"])
@(define sq-open @litchar["["])
@(define sq-close @litchar["]"])

@(define-syntax (racketblockX stx)
   (syntax-case stx ()
     [(_ e ...)
      #`(racketblock #,@(strip-context #'(e ...)))]))

@(define plait-eval (make-base-eval #:lang 'plait))
@(define racket-eval (make-base-eval #:lang 'racket))

@title[#:tag "model"]{A Model of Expansion}

To better understand how macro expansion works in Racket, let's
approach it the same way as in a traditional programming-languages
course: by building an interpreter for a simple Lisp.

@; ----------------------------------------
@section{An Interpreter}

Here's an initial interpeter:

@centerline{@lambda+let.rkt}

Even in the case of implementing this interpreter, we can't help using
a domain-specific variant of Racket. The @defterm{Plait} language (as
referenced by via @racket[@#,hash-lang[] @#,racketmodname[plait]]) is
a statically typed language with polymorphism and type inference. The
type system and semantics of Plait are very close to ML, but the
surface syntax of Plait looks like Racket. That combination makes the
language especially suitable for a programming-language course.

The language that the interpreter implements is the λ-calculus
enriched with numbers, addition, multiplication, @tt{if0}, and
@tt{let}. Let's call this language @defterm{Curly}, since we use curly
braces in place of parentheses to help distinguish the language:

@BNF[(list @nonterm{exp}
           @nonterm{number}
           @nonterm{id}
           @BNF-seq[open @litchar{+} @nonterm{exp} @nonterm{exp} close]
           @BNF-seq[open @litchar{*} @nonterm{exp} @nonterm{exp} close]
           @BNF-seq[open @litchar{lambda} open @nonterm{id} close @nonterm{exp} close]
           @BNF-seq[open @nonterm{exp} @nonterm{exp} close]
           @BNF-seq[open @litchar{if0} @nonterm{exp} @nonterm{exp} @nonterm{exp} close]
           @BNF-seq[open @litchar{let} open sq-open @nonterm{id} @nonterm{exp} sq-close close @nonterm{exp} close])]

Since Curly has no @tt{letrec} form, a Curly programmer must use the Y
combinator to implement a recursive function like @racket[fib]:

@racketblockX[
{let {[Y {lambda {f}
           {{lambda {fX}
              {fX fX}}
            {lambda {fX}
              {f {lambda {x} {{fX fX} x}}}}}}]}
  {let {[fib
         {Y {lambda {fib}
              {lambda {n}
                {if0 n
                     1
                     {if0 {+ n -1}
                          1
                          {+ {fib {+ n -1}}
                             {fib {+ n -2}}}}}}}}]}
    {fib 30}}}
]

So, that kind of program is what the interpreter in
@lambda+let.rkt must evaluate.

Even if the Plait language is not familiar, you'll recognize the
structure of Plait code that interprets Curly programs. It starts with
a datatype for representing Curly expressions:

@racketblock[
(define-type Exp
  (numE [n : Number])
  (idE [s : Symbol])
  (plusE [l : Exp] 
         [r : Exp])
  (multE [l : Exp]
         [r : Exp])
  (lamE [n : Symbol]
        [body : Exp])
  ....)
]

The value of a Curly expression can be either a number or a function,
where a function is represented as closure of an expression and an
environment:

@racketblock[
(define-type Value
  (numV [n : Number])
  (closV [arg : Symbol]
         [body : Exp]
         [env : Env]))
]

The @racket[interp] function takes a Curly expression and an
environment, and it returns a value:

@racketblock[
(define (interp [a : Exp] [env : Env]) : Value
  (type-case Exp a
    [(numE n) (numV n)]
    [(idE s) (lookup s env)]
    [(plusE l r) (num+ (interp l env) (interp r env))]
    [(multE l r) (num* (interp l env) (interp r env))]
    [(lamE n body) (closV n body env)]
    ....))
] 

The one part of @lambda+let.rkt that may be less familiar is the
@racket[parse] function, and we'll look at that next.

@; ----------------------------------------
@section{A Parser}

The job of the @racket[parse] function in @lambda+let.rkt is to
take a Curly program in textual form (with curly braces) and produce
its representation as an instance of the @racket[Exp] datatype.
Instead of actually parsing at the character level, however, the Curly
parser takes advantage of Plait's @defterm{S-expressions}. It also
takes advantage of the fact that @litchar["{"] and @litchar["}"] can
be used as synonyms for @litchar["("] and @litchar[")"] when writing
an S-expression.

An S-expression Plait is written with a backquote: @litchar{`}. After
the backquote is a number, boolean, string, identifier, or
parenthesized form. For example,

@racketblock[`10]

is an S-expression that encapsulates the number @racket[10], while

@racketblock[`apple]

encapsulates the symbol @racket['apple]. Although the first of those
S-experssions has a number inside and the second has a symbol, both
expressions have type @racket[S-Exp] in Plait.

Th S-expression

@racketblock[`(lambda (x) 10)]

which can be equivalently written

@racketblock[`{lambda {x} 10}]

encapsulates a list. The list has three S-expressions inside:
@racket[`lambda], @racket[`{x}], and @racket[`10].

Plait's operations on S-expressions include a way to check whether an
S-expression has a number, @racket[s-exp-number?], and a way to
extract the number, @racket[s-exp->number]:

@examples[#:eval plait-eval #:label #f
(s-exp-number? `apple)
(s-exp-number? `10)
(s-exp->number `10)
]

There are also @racket[s-exp-symbol?], @racket[s-exp->symbol],
@racket[s-exp-list?], and @racket[s-exp->list]:

@examples[#:eval plait-eval #:label #f
(s-exp-list? `{lambda {x} 10})
(first (s-exp->list `{lambda {x} 10}))
]

So, if we put a @litchar{`} in front of the Curly implementation of
@racket[fib] shown earlier, then we have an S-expression that we can
pass to @racket[parse]. The @racket[parse] function can use functions
like @racket[s-exp-number?], @racket[s-exp-list?], and
@racket[s-exp->list] to recursively pull the S-expression apart and
convert it to an @racket[Exp]:

@racketblock[
(define (parse [s : S-Exp]) : Exp
  (cond
    [(s-exp-number? s) (numE (s-exp->number s))]
    [(s-exp-symbol? s) (idE (s-exp->symbol s))]
    (code:comment "check for `{+ <exp> <exp>}:")
    [(and (s-exp-list? s)
          (= 3 (length (s-exp->list s)))
          (s-exp-symbol? (first (s-exp->list s)))
          (symbol=? '+ (s-exp->symbol (first (s-exp->list s)))))
     (plusE (parse (second (s-exp->list s)))
            (parse (third (s-exp->list s))))]
    ....))
]

The check to recognize a @racket[`{+ @#,nonterm{expr}
@#,nonterm{expr}}] S-expression is especially tedious. Plait includes
one more S-expression helper for parsing S-expressions:
@racket[s-exp-match?]. The @racket[s-exp-match?] function takes two
S-expressions, where the first one is used as a pattern and the second
one is matched against the pattern. In the pattern S-expression, the
special symbol @racket['ANY] matches any S-expression,
@racket['SYMBOL] matches any S-expression, and so on, and non-special
symbols must match literally. So,

@racketblock[
(s-exp-match? `{+ ANY ANY} s)
]

is equivalent to

@racketblock[
(and (s-exp-list? s)
     (= 3 (length (s-exp->list s)))
     (s-exp-symbol? (first (s-exp->list s)))
     (symbol=? '+ (s-exp->symbol (first (s-exp->list s)))))
]

Using @racket[s-exp-match?] makes @racket[parse] easier to understand:

@racketblock[
(define (parse [s : S-Exp]) : Exp
  (cond
    [(s-exp-match? `NUMBER s) (numE (s-exp->number s))]
    [(s-exp-match? `SYMBOL s) (idE (s-exp->symbol s))]
    [(s-exp-match? `{+ ANY ANY} s)
     (plusE (parse (second (s-exp->list s)))
            (parse (third (s-exp->list s))))]
    [(s-exp-match? `{* ANY ANY} s)
     (multE (parse (second (s-exp->list s)))
            (parse (third (s-exp->list s))))]
    [(s-exp-match? `{lambda {SYMBOL} ANY} s)
     (lamE (s-exp->symbol (first (s-exp->list 
                                  (second (s-exp->list s)))))
           (parse (third (s-exp->list s))))]
    ....))
]

Note that after recognizing the shape of a @tt{lambda} form, pulling
out the relevant parts is still a little tedious, so there's room for
an even better pattern-and-template language. But simple pattern
matching is good enough for now.

The implementation of @lambda+let.rkt is missing most of its tests, but it
includes at least one test to show how @racket[parse] works:

@racketblock[
  (test (parse `{let {[x {+ 1 2}]}
                  y})
        (letE 'x (plusE (numE 1) (numE 2))
              (idE 'y)))
]

The tests for @racket[interp] use @racket[parse] for convenience:

@racketblock[
  (test (interp (parse `{+ {* 2 3} {+ 5 8}})
                mt-env)
        (numV 19))
]

@; ----------------------------------------
@section{Desugaring}

The interpreter in @lambda+let.rkt handles @tt{let} as a core form,
but a @tt{let} form is trivially converted into a @tt{lambda} and
application form:

@racketblock[
{let {[@#,nonterm{id} @#,nonterm{rhs-exp}]}
  @#,nonterm{body-exp}}
=
{{lambda {@#,nonterm{id}} @#,nonterm{body-exp}}
 @#,nonterm{rhs-exp}}
]

In other words, @tt{let} is @defterm{syntactic sugar} that can be
replaced with other forms that are already in the language.

Instead of having @racket[interp] handle a @racket[letE] form, we
could instead have @racket[parse] generate an @racket[appE] plus
@racket[lambdaE] instead of @racket[letE]. That way, only
@racket[parse] has to deal with @tt{let} forms. In other words,
@racket[parse] can @defterm{desugar} source programs as it parses:

@racketblock[
(define-type Exp ....) (code:comment "no letE case, anymore")

(define (parse [s : S-Exp]) : Exp
  (cond
    ....
    [(s-exp-match? `{let {[SYMBOL ANY]} ANY} s)
     (let ([bs (s-exp->list (first
                             (s-exp->list (second
                                           (s-exp->list s)))))])
       (appE (lamE (s-exp->symbol (first bs))
                   (parse (third (s-exp->list s))))
             (parse (second bs))))]
    ....))
]

This desugaring approach is better than handling @tt{letE} in the rest
of the program, but it still duplicates information in the sense that
it repeats the encoding of @tt{lambda} and application in terms of
@tt{appE} and @tt{lambdaE}. We could avoid that duplication by
converting a @tt{let} S-expression to an application and @tt{lambda}
S-expression and then recurring:

@racketblock[
(define (parse [s : S-Exp]) : Exp
  (cond
    ....
    [(s-exp-match? `{let {[SYMBOL ANY]} ANY} s)
     (let ([bs (s-exp->list (first
                             (s-exp->list (second
                                           (s-exp->list s)))))])
       (parse (list->s-exp
               (list (list->s-exp
                      (list `lambda
                            (list->s-exp
                             (list (first bs)))
                            (third (s-exp->list s))))
                     (second bs)))))]
    ....))
]

This approach is conceptually nicer, but all the list constructions
and @racket[list->s-exp] coercions are painful.

@; ----------------------------------------
@section{Quasiquotation}

Prefixing a parenthesized form with @litchar{`} quotes it so that no
evaluation takes place for sub-forms, even if they look like Plait
expressions that could be evaluated.

@examples[#:eval plait-eval #:label #f
`(lambda (x) (+ 1 2))
]

If we @emph{want} to evaluate a sub-form and treat its result as part
of the enclosing S-expression, then we can escape back to Plait by
using the unquote operator @litchar{,} as an escape. The result of an
escaped expression must be an S-expression so that it can be part of
the enclosing S-expression.

@examples[#:eval plait-eval #:label #f
`(lambda (x) ,(number->s-exp (+ 1 2)))
]

Unquoting makes @tt{let} desugaring much easier to implement and read,
especially because nested parts extracted from an existing
S-expression are already S-expressions:

@racketblock[
(define (parse [s : S-Exp]) : Exp
  (cond
    ....
    [(s-exp-match? `{let {[SYMBOL ANY]} ANY} s)
     (let ([bs (s-exp->list (first
                             (s-exp->list (second
                                           (s-exp->list s)))))])
       (parse `{{lambda {,(first bs)} ,(third (s-exp->list s))}
                ,(second bs)}))]
    ....))
]

Here's the updated interpeter:

@centerline{@lambda.rkt}

Quasiquotation (i.e., quoting with unquoting to escape) is even more
convenient in an untyped language like Racket where lists, symbols,
and numbers can be treated directly as S-expressions without coercion.
Here are some examples in Racket (not Plait):

@examples[#:label #f
`(1 2 3 4)
`(1 ,(+ 2 3) 4)
(define (make-adder n)
  `(lambda (x) (+ x ,n)))
(make-adder 5)
(make-adder `(current-seconds))
]

Since plain Racket symbols and lists are S-expressions, Racket's plain
quoting operator @litchar{'} (which does not support escapes) also
creates S-expressions.

In addition to the unquote operator @litchar{,}, both Racket and Plait
support a @defterm{splicing unquote} operator @litchar[",@"]. A splicing
unquote expects the escaped expression to produce a list, and it
splices all of the individual elements of the list in place of the
escape.

@examples[#:label #f
`(1 ,@(list 2 3) 4)
`(1 ,(list 2 3) 4)
(define (make-adder* ns)
  `(lambda (x) (+ x ,@ns)))
(make-adder* `(5 (current-seconds)))
]

The @litchar{'}, @litchar{`}, @litchar{,}, and @litchar[",@"] operators
are actually shorthands for @racket[quote], @racket[quasiquote],
@racket[unquote], and @racket[unquote-splicing], respectively.

@examples[#:label #f
(eval:alts '(@#,racket[quote] apple) '(quote apple))
(eval:alts '(@#,racket[quasiquote] apple) '(quasiquote apple))
(eval:alts '(@#,racket[unquote] apple) '(unquote apple))
(eval:alts '(@#,racket[unquote-splicing] apple) '(unquote-splicing apple))
]

@; ----------------------------------------
@section{Macros}

We can build as much desugaring as we want into @racket[parse] to
create an ever richer Curly language. Or we can give programmers the
tools to add those extensions themselves by adding macros to Curly. A
macro is implemented by a @defterm{macro transformer} that takes an
S-expression (such as the S-expression for a @tt{let} form) and
converts it to a different S-expression (such as an S-expression that
uses application and @tt{lambda}, instead). Instead of implementing
that transformation in Plait as part of the Curly implementation, a
macro transformer is implemented in Curly to be used on later Curly
expressions.

Adding macros to Curly requires two sets of changes:

@itemlist[

 @item{We must add list and symbol values and operations on them, so
       they can be used for S-expressions. Since Curly is untyped, we
       won't have to distinguish S-expressions from lists, symbols,
       and numbers.}

 @item{We must adjust @racket[parse] to have an environment mapping
       names to macro transformers. It must also recognize a
       @tt{let-macro} form, which maps a macro name to a transformer
       that is implemented in Curly; @racket[parse] will need to
       directly call @racket[interp] to evaluate the right-hand side
       of @tt{let-macro}, and then @racket[parse] can call itself
       again for the body of @tt{let-macro} using an updated macro
       environment.}

]

While quasiquoting is convenient, it's not essential, so we'll just
add @tt{quote} forms, which can be written either with @tt{quote} or
using the @litchar{'} shorthand. Quoting provides a way to write
symbol literals and the empty list, and it also supports more general
literal lists and S-expressions.

@BNF[(list @nonterm{exp}
           @t{....}
           @BNF-seq[open @litchar{quote} @nonterm{s-exp} close]
           @BNF-seq[open @litchar{cons} @nonterm{exp} @nonterm{exp} close]
           @BNF-seq[open @litchar{first} @nonterm{exp} close]
           @BNF-seq[open @litchar{rest} @nonterm{exp} close]
           @BNF-seq[open @litchar{empty?} @nonterm{exp} close])
     (list @nonterm{s-exp}
           @nonterm{number}
           @nonterm{id}
           @BNF-seq[open @kleenestar{@nonterm{s-exp}} close])]

Each result value is now either a number, a closure, a symbol, or a
list of values:

@racketblock[
(define-type Value
  (numV [n : Number])
  (closV [arg : Symbol]
         [body : Exp]
         [env : Env])
  (symV [s : Symbol])
  (listV [l : (Listof Value)]))
]

For macro binding, we add @tt{let-macro} to the grammar:

@BNF[(list @nonterm{exp}
           @t{....}
           @BNF-seq[open @litchar{let-macro} open sq-open @nonterm{id} @nonterm{exp} sq-close close @nonterm{exp} close])]

The @racket[parse] function will expect the first @nonterm{exp} in
@tt{let-macro} to produce a function, but that constraint is not
expressed in the grammar.

Putting these pieces together, here's a Curly program that defines its
own @tt{let} macro, and then uses it to bind @tt{x}:

@racketblockX[
{let-macro {[let {lambda {s}
                   {cons {cons 'lambda
                               {cons
                                {cons {first {first {first {rest s}}}}
                                      '{}}
                                {cons {first {rest {rest s}}}
                                      '{}}}}
                         {cons {first {rest {first {first {rest s}}}}}
                               '{}}}}]}
   {let {[x 5]}
     {+ x x}}}
]

One advantage of making Curly's syntax a subset of Racket is that we
can extract out just the transformer expression and try it as a plain
Racket function:

@examples[#:label #f #:eval racket-eval
(let ([let-transformer
       (code:comment "same as above")
       {lambda {s}
         {cons {cons 'lambda
                     {cons
                      {cons {first {first {first {rest s}}}}
                            '{}}
                      {cons {first {rest {rest s}}}
                            '{}}}}
               {cons {first {rest {first {first {rest s}}}}}
                     '{}}}}])
  (code:comment "apply it to an S-expression")
  (let-transformer
   '{let {[x 5]} {+ x x}}))
]

The full implementation of Curly with macros is

@centerline{@let-macro.rkt}

Here are the most relevant parts of @racket[parse]:

@racketblock[
(define (parse* [s : S-Exp] [env : Env]) : Exp
  (cond
    [(s-exp-match? `{let-macro {[SYMBOL ANY]} ANY} s)
     (let ([bs (s-exp->list (first
                             (s-exp->list (second
                                           (s-exp->list s)))))])
       (let ([proc (interp (parse (second bs))
                           mt-env)])
         (parse* (third (s-exp->list s))
                 (extend-env (bind (s-exp->symbol (first bs))
                                   proc)
                             env))))]
    ....
    [(s-exp-match? `{ANY ANY ...} s)
     (let ([rator (first (s-exp->list s))])
       (code:comment "try as macro...")
       (try
        (parse* (apply-macro (lookup (s-exp->symbol rator) env)
                             s)
                env)
        (code:comment "... fall back to application")
        (lambda ()
          (if (= (length (s-exp->list s)) 2)
              (appE (parse* rator env)
                    (parse* (second (s-exp->list s)) env))
              (error 'parse "invalid input")))))]
     ....))
]

@; ----------------------------------------
@section{Exercises}

Start with @|let-macro.rkt|.

@itemlist[
#:style 'ordered

@item{Fill in the @tt{let-macro}-bound implementation of @tt{-} in the
following program so that it produces @racketresultfont{3}:

@racketblock[
        (interp
         (parse
          `{let-macro {[- {lambda {s}
                            ....}]}
             {- 7 4}})
         mt-env)
]

You can define @tt{-} in terms of @tt{+}, @tt{*}, and @tt{-1}. Note
that this @tt{-} cannot be defined as a function, since Curly supports
only single-argument functions and application.}

@item{Having only @tt{cons} instead of @tt{list} makes the
implementation of macros in Curly relatively difficult. Replace
@racket[....] in the following test case with the implementation of a
@tt{list} macro so that the test passes:

@racketblock[
  (test (interp
         (parse
          `{let-macro {[let {lambda {s}
                              {let-macro {[list ....]}
                                {list
                                 {list 'lambda
                                       {list {first {first {first {rest s}}}}}
                                       {first {rest {rest s}}}}
                                 {first {rest {first {first {rest s}}}}}}}}]}
             {let {[x 5]}
               {+ x x}}})
         mt-env)
        (numV 10))
]

@bold{Hint:} You can make a macro recursive by having one step of
expansion produce an expression that refers to the macro again. For
example, @tt{{list 1 2 3}} could expand to @tt{{cons 1 {list 2 3}}}.

@bold{Hint:} Remember to use @tt{if0} to branch on the result of
@tt{empty?} in Curly.

@bold{Food for thought:} Why is @tt{list} defined inside the
@tt{lambda} for the implementation of @tt{let}?}

]
