
all:
	raco make notes.scrbl
	raco scribble --htmls +m --redirect-main "https://docs.racket-lang.org/" notes.scrbl

upload:
	rsync -r -v notes/ shell.cs.utah.edu:plt-web/dagstuhl19
