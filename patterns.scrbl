#lang scribble/manual
@(require scribble/example
          (for-label racket/base
                     syntax/parse
                     (only-in syntax/parse/define define-simple-macro)))

@(define stx-parse-eval (make-base-eval))
@examples[#:hidden #:eval stx-parse-eval (require syntax/parse)]

@title[#:tag "patterns"]{Patterns and Templates}

While @racket[quasisyntax] is usually a lot better than @racket[list]
and @racket[datum->syntax] to construct syntax objects, it doesn't
help at all for parsing the input syntax object to a transformer. The
@racket[syntax-parse] form from the @racket[syntax/parse] library
provides much better support for both parsing a syntax object and
constructing a new one. It's derived from pattern-based macro tools
that started in Scheme.

@; ----------------------------------------
@section{Introducing @racket[syntax-parse]}

The basic form of a @racket[syntax-parse] expression is

@racketblock[
(syntax-parse _stx-exp
  [_pattern _result-exp]
  ...)
]

In a @racket[_pattern], identifiers generally stand for ``any syntax
object,'' and the identifier is bound as a @defterm{pattern variable}
in the @racket[_result-exp]. The value of a pattern variable is the
part of the @racket[_stx-exp] syntax object that it matched. A
pattern variable cannot be referenced directly; it can only be used in
a @defterm{template}, which is written with @litchar{#'} or
@racket[syntax].@margin-note*{The @racket[syntax] form cooperates with
forms like @racket[syntax-parse] to support pattern variables. Until
this point, we have used @racket[syntax] in a more primitive way. The
actual primitive form, which does not support pattern variables, is
called @racket[quote-syntax].}

@examples[#:label #f #:eval stx-parse-eval
(syntax-parse #'(one two three)
  [(a b c) #'a])
(syntax-parse #'(one two three)
  [(a b c) #'c])
(syntax-parse #'(one (two zwei (dos)) three)
  [(a (b1 b2 (b3)) c) #'b3])
]

A template doesn't have to contain just a single pattern variable. It
can refer to multiple pattern variables, and anything that isn't a
pattern variable is kept as-is.

@examples[#:label #f #:eval stx-parse-eval
(syntax-parse #'(one two three)
  [(a b c) #'(alpha (a b) beta ((c) b) a gamma)])
]

When there are multiple @racket[_pattern]s, @racket[syntax-parse]
looks for the first match.

@examples[#:label #f #:eval stx-parse-eval
(syntax-parse #'(one two three)
  [(a b) #'a]
  [(a b c) #'b]
  [(a b c d) #'d])
]

The identifier @racket[_] is special, because it matches anything
without binding a pattern variable.

@examples[#:label #f #:eval stx-parse-eval
(syntax-parse #'(one two three)
  [(a _) #'a]
  [(_ b _) #'b]
  [(_ _ _ d) #'d])
]

Another special identifier is @racket[~literal], which wraps an
identifier within a pattern to make it match a literal identifier,
instead of treating the identifier as a pattern variable.

@examples[#:label #f #:eval stx-parse-eval
(syntax-parse #'(one two three)
  [(_ (~literal two) c) #'c]
  [(a (~literal zwei) _) #'a])
(syntax-parse #'(ein zwei drei)
  [(_ (~literal two) c) #'c]
  [(a (~literal zwei) _) #'a])
]

@; ----------------------------------------
@section{Using @racket[syntax-parse]}

Let's revisit the @racket[time-it] example, but with
@racket[syntax-parse].

@racketmod[
racket/base
(require (for-syntax racket/base
                     syntax/parse))

(define (time-thunk thunk)
  (let* ([before (current-inexact-milliseconds)]
         [answer (thunk)]
         [after (current-inexact-milliseconds)])
   (printf "It took ~a ms to compute.\n" (- after before))
   answer))
 
(define-syntax (time-it stx)
  (syntax-parse stx
    [(_ expr)
     #'(time-thunk (lambda () expr))]))
 
(time-it (+ 1 2))
]

Besides being easier to read than our earlier implementations, note
that @racket[time-it] reports a helpful error message if it is wrapped
about more than one expression.

@; ----------------------------------------
@section{Ellipses}

A @racket[syntax-parse] pattern can include @racket[...] to mean ``any
number of repetitions.'' It applies to the sub-pattern that immediate
precedes the @racket[...]:

@examples[#:label #f #:eval stx-parse-eval
(syntax-parse #'(one two three)
  [(_ ...) 'ok])
(syntax-parse #'(one (two) (three))
  [((_) ...) 'first]
  [(_ (_) ...) 'second])
(syntax-parse #'((one) (two zwei dos) (three drei tres troi))
  [((_ ...) ...) 'ok])
]

When a pattern variable is repeated by @racket[...], then the pattern
variable is bound to a sequence of matches, instead of just one match.
When the pattern variable is referenced in a template, a @racket[...]
must appear after the reference to consume the sequence of matches. At
least one sequence-bound pattern variable must appear before
@racket[...] in a template, and anything else in the sub-template
before @racket[...] is duplicated for each element of the sequence.

@examples[#:label #f #:eval stx-parse-eval
(syntax-parse #'(one (two) (three))
  [(_ (b) ...) #'(pre b ... post)])
(syntax-parse #'(one (two) (three))
  [(_ (b) ...) #'((pre b post) ...)])
]

Since the @racket[run] macro is supposed to allow any number of
argument, ellipses are needed for its pattern form:

@RACKETBLOCK[
(define-syntax (run stx)
  (syntax-parse stx
    [(_ proc arg ...)
     #`(run-program (symbol->string 'proc)
                    (symbol->string 'arg) ...)]))
]

While this variant of the macro is convenient to write, it defers
@racket[symbol->string] to run time. To perform the conversion at
compile time, it makes sense to mix pattern matching and
@racket[quasisyntax]:

@RACKETBLOCK[
(define-syntax (run stx)
  (syntax-parse stx
    [(_ proc arg ...)
     #`(run-program #,(symbol->string (syntax-e #'proc))
                    #,@(map symbol->string
                            (map syntax-e
                                 (syntax->list #'(arg ...)))))]))
]

Another way to write @racket[run] is to use @racket[syntax-parse]'s
@racket[#:where] directive. A @racket[#:where] is written within one
pattern--result clause, and it is followed by two forms: a pattern and
an expression. The pattern is matched against the result of the
expression, and any pattern variables bound by the pattern can be used
in the remainder of the clause.

@RACKETBLOCK[
(define-syntax (run stx)
  (syntax-parse stx
    [(_ proc arg ...)
     #:with proc-str (symbol->string (syntax-e #'proc))
     #:with (arg-str ...) (map symbol->string
                                (map syntax-e
                                     (syntax->list #'(arg ...))))
     #`(run-program 'proc-str 'arg-str ...)]))
]

Unlike @racket[quasisyntax], which uses something like @racket[#'here]
as the first argument to @racket[datum->syntax] to coerce to a syntax
object, @racket[#:with] uses @racket[#f]. It turns out that binding
matters even for strings when they are in expression position, so the
above variant of @racket[run] compensates by using an explicit
@racket[quote] around each string.

@; ----------------------------------------
@section{Syntax Classes}

Although our latest variants of @racket[run] report a good error in
some cases, such as when no program and arguments are provided, the
error message is bad if a program or argument is anything other than
an identifier. To make @racket[syntax-parse] provide a good error
message in those cases, we can annotate the pattern variables with the
@defterm{syntax class} @racket[id], which matches only identifiers.
Annotate a pattern variable with a syntax class by using @litchar{:}
followed by the syntax class @bold{without} spaces around @litchar{:}.

@RACKETBLOCK[
(define-syntax (run stx)
  (syntax-parse stx
    [(_ proc:id arg:id ...)
     #`(run-program #,(symbol->string (syntax-e #'proc))
                    #,@(map symbol->string
                            (map syntax-e
                                 (syntax->list #'(arg ...)))))]))
]

The @racket[id] syntax class is predefined, but @racket[syntax-parse]
enables programmers to define new syntax classes with
@racket[define-syntax-class]. Syntax classes not only specify
constraints on pattern matching, but they can also synthesize
attributes; they work in general as composable parsers. We don't have
time to get into those details here, though.

@; ----------------------------------------
@section{Simple Macros}

Many macros are simple like @racket[time-it]: they have one pattern
followed immediately by the result template. For those cases,
@racketmodname[syntax/parse/define] provides
@racket[define-simple-macro]. With @racket[define-simple-macro], the
pattern is incorporated into the definition header (so that the
underlying syntax-object argument is not named), and no @litchar{#'}
is needed before the result template.

@racketmod[
racket/base
(require syntax/parse/define)

(define (time-thunk thunk) ....)
 
(define-simple-macro (time-it expr)
  (time-thunk (lambda () expr)))
 
(time-it (+ 1 2))
]

Note that @racketmodname[syntax/parse/define] is @racket[require]d
without @racket[for-syntax], since it binds
@racket[define-simple-macro] for run-time definition positions---even
though @racket[define-simple-macro]'s work is at compile time.

@; ----------------------------------------
@section[#:tag "pattern-exercises"]{Exercises}

@itemlist[
#:style 'ordered

@item{Define @racket[define-five] with run-time warnings as a
pattern-based macro using @racket[define-simple-macro].

Probably unlike your earlier @racket[define-five], this one should
provide a good error message if the form after @racket[define-five] is
not an identifier.}


@item{Define @racket[define-five] with compile-time warnings as a
pattern-based macro.

@bold{Hint:} You may not want @racket[define-simple-macro]. Or you can
use the @racket[#:do] pattern directive; see
@secref["Pattern_Directives" #:doc '(lib
"syntax/scribblings/syntax.scrbl")].}

@item{Define a @racket[(result .... where ....)] form that has local
bindings written after an expression that uses them. For example,

@racketblock[
(result (+ x y)
        where
        (define x 1)
        (define y 2))
]

should be equivalent to @margin-note*{Using an empty @racket[let] is
an idiomatic Racket way of creating a nested definition context.}

@racketblock[
(let ()
  (define x 1)
  (define y 2)
  (+ x y))
]

The @racket[result] macro should require literally the word
@racket[where] after the result expression. The @racket[result] form
should also allow exactly one result expression but any number of
definitions.}

]
