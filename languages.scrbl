#lang scribble/manual
@(require (for-label racket/base
                     syntax/parse))

@title[#:tag "languages"]{Languages}

The @hash-lang[] that starts a Racket-program file determines what the
rest of the file means. Specifically, the identifier immediately after
@hash-lang[] selects an meaning for the rest of the file, and it gets
control at the character level. The only constraint on a
@hash-lang[]'s meaning is that it denotes a Racket module that can be
referenced using the file's path. That module is obligated to define
certain things to star the language's implementation.

@; ----------------------------------------
@section[#:tag "modules-to-langs"]{From Modules to Languages}

It turns out that you can write the primitive @racket[module] form
directly in DrRacket. If you leave out any @hash-lang[] line and write

@racketblock[
(module example racket/base
  (#%module-begin
    (+ 1 2)))
]

then it's the same as

@racketmod[
racket/base
(+ 1 2)
]

and if you write the latter form, then it essentially turns into the
former form. Both forms have the same @racket[(+ 1 2)] because
@racket[@#,hash-lang[] racket] uses the native syntax for the module
body.

@nested[#:style 'inset]{Technically, there's a difference in intent in
the above two chunks of text showing programs. In the second case
witth @hash-lang[], the parentheses are meant as actual parenthesis
characters that reside in a file. In the first case with
@racket[module], the parentheses are just a way to write a text
representation of the actual value, which is a syntax object that
contains a lists of syntax objects that contain symbols, and so on. A
language implementation has to actually parse the parentheses in the
second block of code to produce the first.}

Let's define a language for running external programs that supports
the @racket[run] form and nothing else. We'll define @racket[pfsh] so
that

@racketmod[
@#,racket[pfsh]
(whoami)
(ls -l)
(echo Hello)
]

corresponds to

@racketblock[
(module example pfsh
  (#%module-begin
   (whoami)
   (ls -l)
   (echo Hello)))
]

For now, we don't want to bother parsing at the level of parentheses, so we'll
actually write
@;
@filebox["example.rkt"
@racketmod[
s-exp "pfsh.rkt"
(whoami)
(ls -l)
(echo Hello)
]]
@;
The @racketmodname[s-exp] language doesn't do anything but parse
parentheses into syntax objects. For this example, it directly generates the syntax
object
@;
@racketblock[
(module example "pfsh.rkt"
  (#%module-begin
   (whoami)
   (ls -l)
   (echo Hello)))
]
@;
@margin-note{Without creating a @filepath{pfsh.rkt} file, copy the
@racket[@#,hash-lang[] s-exp "pfsh.rkt"] example into DrRacket
and click the @onscreen{Macro Stepper} button. The stepper will
immediately error, since there's no @filepath{pfsh.rkt} module, but
it will show you the parsed form.}
@;
which is half-way to where we want to be: the @racket[whoami]
and @racket[echo] syntax objects are still here to be
expanded by macros, but we no longer have to worry about parsing
characters. (The change from @racket[pfsh] to
@racket["pfsh.rkt"] just lets us work with relative paths, for now,
instead of installing a @tt{pfsh} collection.)

@; ----------------------------------------
@section[#:tag "core-module"]{The Core @racket[module] Form}

The core @racket[module] grammar is
@;
@racketgrammar*[#:literals [module #%module-begin]
 (Module (module name initial-import-module
           (#%module-begin
             form ...))
         (module _name _initial-import-module
             form ...))
]
@;
The second variant is a shorthand for the first, and it is
automatically converted to the first variant by adding
@racket[#%module-begin].

For a module that comes from a file, the @racket[_name] turns out to
be ignored, because the file path acts as the actual module name. The
key part is @racket[_initial-import-module]. The module named by
@racket[_initial-import-module] gives meaning to some set of
identifiers that can be used in the module body. There are absolutely
@emph{no} pre-defined identifiers for the body of a module. Even
things like @racket[lambda] or @racket[#%module-begin] must be
exported by @racket[_initial-import-module] if they are going to be
used in the module body's @racket[_form]s.

If @racket[require] is provided by @racket[_initial-import-module],
then it can be used to pull in additional names for use by
@racket[_form]s. If there's no way to get at @racket[require],
@racket[define], or other binding forms from the exports of
@racket[_initial-import-module], then nothing but the exports of
@racket[_initial-import-module] will ever be available to the
@racket[_form]s.

Since every @racket[module] for has an explicit or implicit
@racket[#%module-begin], @racket[_initial-import-module] had better
provide @racket[#%module-begin]. If a language should allow the same
sort of definition-or-expression sequence as @racketmodname[racket],
then it can just re-export @racket[#%module-begin] from
@racketmodname[racket]. As we will see, there are some other implicit
forms, all of which start with @litchar{#%}, and
@racket[_initial-import-module] must provide those forms if they're
going to be triggered.

Here is the simplest possible Racket language module:
@;
@filebox["simple.rkt"
@codeblock{
 #lang racket/base
 (provide #%module-begin)
}]
@;
Since @filepath{simple.rkt} provides @racket[#%module-begin], it's a
valid initial import. You can use it in the empty program
@;
@filebox["use-simple.rkt"
@codeblock{
#lang s-exp "simple.rkt"
}]
@;
as long as @filepath{use-simple.rkt} is saved in the same directory as
@filepath{simple.rkt} (so that the relative path works). You can add
comments after the @hash-lang[] line, since comments are stripped away
by the parser. Nothing else in the body is going to work, though.
@margin-note*{Actually, @racket[(#%module-begin)] will work, since
@racket[#%module-begin] is bound and since @racketmodname[s-exp]
relies on the implicit introduction of @racket[#%module-begin] instead
of adding it explicitly. That's a flaw in @racketmodname[s-exp].}

@; ----------------------------------------
@section[#:tag "pfsh0"]{Modules and Renaming}

Let's create a variant @racket["pfsh0.rkt"] that has a @racket[run]
form to run an external program:

@racketblock[
@#,hash-lang[] s-exp "pfsh0.rkt"
(run ls -l)
]

Since that's equivalent to

@racketblock[
(module example "pfsh0.rkt"
  (#%module-begin
   (run ls -l)))
]

then we need to create a @filepath{pfsh0.rkt} module that provides
@racket[#%module-begin] and @racket[run]. The @racket[run] macro's job
is to treat its identifiers as strings and deliver them to the
@racket[run] function that we defined in @filepath{run.rkt}:

@filebox["pfsh0.rkt"
@racketmod[
racket/base
(require "run.rkt"
         (for-syntax racket/base
                     syntax/parse))

(provide #%module-begin
         (rename-out [pfsh:run run]))

(define-syntax (pfsh:run stx)
  (syntax-parse stx
    [(_ prog:id arg:id ...)
     #'(void (run (symbol->string 'prog) (symbol->string 'arg) ...))]))
]]

We've wrapped @racket[void] around the call to @racket[run] to
suppress the success or failure boolean that would otherwise print
after the run program's output.

@; ----------------------------------------
@section{Adjusting @racket[#%module-begin]}

The biggest difference between the @racket[pfsh] that we've
implemented and the @racket[pfsh] that we want is that we have to put
@racket[run] before every program name. Instead of @racket[(run ls)],
we want to write @racket[(ls)].

Since macros can do any kind of work at compile time, you might
imagine changing @racket[pfsh] so that it scans the filesystem and
builds up a set of definitions based on the programs that are
currently available via the @tt{PATH} environment variable. That's not
how scripting languages are meant to work, though. Also, it's likely
to cause trouble to use the filesystem and environment-variable state
at such a fine granularity to determine bindings of a module.

Another possibility is to change @racket[#%module-begin] so that it
takes every form in the module and adds @racket[run] to the front:

@filebox["pfsh1.rkt"
@racketmod[
racket/base
(require "run.rkt"
         (for-syntax racket/base
                     syntax/parse))
 
(provide (rename-out [pfsh:module-begin #%module-begin]
                     [pfsh:run run]))

(define-syntax (pfsh:module-begin stx)
  (syntax-parse stx
    [(_ (prog:id arg:id ...) ...)
     #'(#%module-begin
        (pfsh:run prog arg ...)
        ...)]))

(define-syntax (pfsh:run stx)
  (syntax-parse stx
    [(_ prog:id arg:id ...)
     #'(void (run (symbol->string 'prog) (symbol->string 'arg) ...))]))
]]

Notice that @racket[pfsh:module-begin] adds @racket[pfsh:run] to the
start of each body form, not just @racket[run]. That's because it
wants to insert a reference to @racket[run] as provided by
@filepath{pfsh1.rkt}, and that form is called @racket[pfsh:run] in the
environment of the @racket[pfsh:module-begin] implementation.

Meanwhile, there's probably no point to exporting @racket[run], since
it can never referenced directly in a module that is implemented with
@racket[@#,hash-lang[] s-exp "pfsh1.rkt"].

@; ----------------------------------------
@section{The Application Form}

While adjusting [#%module-begin] works for @filepath{pfsh1.rkt}, it's
not a very composable approach. If we later want to support more kinds
of forms in the module body, we have to change
@racket[pfsh:module-begin] to recognize each of them.

Instead, we would like to change the default meaning of parentheses.
In Racket, a pair of parentheses mean a function call by default. In
@racket[pfsh], a pair of parentheses should mean running an external
program by default. The ``by default'' part concedes that an
identifier after an open parenthesis can change the meaning of the
parenthesis, such as when @racket[define] appears after an open
parenthesis. Otherwise, though, it's as if a @racket[function-call]
identifier appears after the open parenthesis to specify a
function-call form... and @racket[function-call] exists, except that
it's spelled @racket[#%app].

In other words, in the @racket[racket] language, when you write
@;
@racketblock[(+ 1 2)]
@;
since @racket[+] is not bound as a macro or core syntactic form,
that expands to
@;
@racketblock[(#%app + 1 2)]
@;
The @racket[#%app] provided by @racketmodname[racket] is defined as a
macro that expands to the core syntactic form for function calls. That
core form is also called @racket[#%app] internally, but in the rare
case that we have to refer to the core form, we use the alias
@racket[#%plain-app].

To change the default meaning of parentheses for @racket[pfsh], then,
we can rename @racket[pfsh:run] to @racket[#%app] on export:
@;
@filebox["pfsh2.rkt"
@racketmod[
racket/base
(require "run.rkt"
         (for-syntax racket/base
                     syntax/parse))
 
(provide #%module-begin
         (rename-out [pfsh:run #%app]))

(define-syntax (pfsh:run stx)
  (syntax-parse stx
    [(_ prog:id arg:id ...)
     #'(void (run (symbol->string 'prog) (symbol->string 'arg) ...))]))
]]
@;
After that small adjustment, we conceptually change each @racket[run]
in a @racket[pfsh] module to @racket[#%app], but we don't actually
have to write the @racket[#%app], since it's added automatically by the
expander:
@;
@filebox["use-pfsh2.rkt"
@racketmod[
s-exp "pfsh2.rkt"
(whoami)
(ls -l)
(echo Hello)
]]

@;----------------------------------------
@section{Installing a Language}

Let's take the last step in defining a language, which will let use
switch from @racket[@#,hash-lang[] s-exp "pfsh2.rkt"] to
@racket[@#,hash-lang[] pfsh]. To enable writing @racket[@#,hash-lang[]
pfsh], we must do two things:

@itemlist[

 @item{Adjust our language implementation so that it explicitly
       specifies S-expression parsing, instead of having S-expression
       parsing imposed externally.}

 @item{Install our language as a package so that
       @racket[@#,hash-lang[] pfsh] will work from anywhere.}

]

The part of a language that specifies its parsing from characters to
syntax objects is called a @deftech{reader}. A language's reader is
implemented by a @racket[reader] submodule (i.e., a nested module)
inside the language's module. That submodule must export a
@racket[read-syntax] function that takes an input port, reads
characters from it, and constructs a @racket[module] form as a syntax
object. For historical reasons, the submodule should also provide a
@racket[read] function that does the same thing but returns a plain
S-expression instead of a syntax object.

Here's one way to implement the @racket[reader] submodule:
@;
@racketblock[
(module reader racket
  (provide (rename-out [pfsh:read-syntax read-syntax]
                       [pfsh:read read]))

  (define (pfsh:read-syntax name in)
    (datum->syntax #f `(module anything pfsh
                         (#%module-begin
                          ,@(read-body name in)))))

  (define (read-body name in)
    (define e (read-syntax name in))
    (if (eof-object? e)
        '()
        (cons e (read-body name in))))

  (define (pfsh:read in)
    (syntax->datum (pfsh:read-syntax 'src in))))
]

Notice that @racket[pfsh:read-syntax] constructs a module that uses
@racket[pfsh] as the initial import. Otherwise, it doesn't really do
anything specific to @racket[pfsh], and most of the work is performed
by the built-in @racket[read-syntax] function that reads a single term
(such an an identifier or parenthesized form) as a syntax object. In
fact, since this pattern is so common, Racket provides a
@racket[syntax/module-reader] language that expects just the
@racket[pfsh] part and builds the rest of the submodule around that.
#;
@racketblock[
(module reader syntax/module-reader
  pfsh)
]

In short, we just need to add those two lines to our current
@racket[pfsh] implementation, and then save it as @filepath{main.rkt}
in a @filepath{pfsh} directory. Here's the complete implementation:

@filebox[@elemtag["pfsh/main.rkt"]{@filepath{pfsh/main.rkt}}
@racketmod[#:escape UNSYNTAX
racket/base
(require "run.rkt"
         (for-syntax racket/base
                     syntax/parse))
 
(provide #%module-begin
         (rename-out [pfsh:run #%app]))

(module reader syntax/module-reader
  pfsh)

(define-syntax (pfsh:run stx)
  (syntax-parse stx
    [(_ prog:id arg:id ...)
     #'(void (run (symbol->string 'prog) (symbol->string 'arg) ...))]))
]]

You'll also need @elemref["run.rkt"]{@filepath{run.rkt}} in the same
@filepath{pfsh} directory.

To install this as a package, select @onscreen{Install Package...}
from the DrRacket @onscreen{File} menu, click the @onscreen{Browse}
button to select a @onscreen{Directory}, and select the
@filepath{pfsh} directory. Alternatively, run
@;
@commandline{raco pkg install pfsh/}
@;
on the command line---and beware that the trailing slash is necessary
(otherwise, @exec{raco pkg} will consult a remote server to look for a
registered @racket[pfsh] package).

After either of those steps, you can run

@racketmod[
@#,racket[pfsh]
(echo Hello!)
]

@; ----------------------------------------
@section[#:tag "language-exercises"]{Exercises}

Start with either @filepath{pfsh2.rkt} or @filepath{pfsh/main.rkt},
depending on whether you want to install @filepath{pfsh} as a package.

@itemlist[
#:style 'ordered

@item{Our @racket[pfsh] language so far only allows @racket[run] as an
application form. Change pfsh to supply all of
@racketmodname[racket/base], but with @racket[#%app] as
@racket[pfsh:run].

A module can export everything that it imported from another module
using @racket[(all-from-out _module)] in @racket[provide]. Also, for
any @racket[_provide-spec] within @racket[provide],
@racket[(except-out _provide-spec _id ...)] is the same as
@racket[_provide-spec], but omitting the @racket[id]s.

After updating @racket[pfsh], this program should work and print
@racketresult['done] after listing files:

@racketmod[
@#,racket[pfsh]

(define done-sym 'done)
(ls -l)
done-sym
]}

@item{After the previous exercise, nearly all of
@racketmodname[racket/base] is available---but difficult to use,
because function application is always turned into a shell command.

A more useful language would treat a parenthesized form as a shell
command only if the function position is an unbound identifier. Simple
pattern matching can distinguish identifiers from non-identifiers, but
determining binding requires more help from the macro expander. The
@racket[identifier-binding] function provides that cooperation; it
takes an identifier and returns @racket[#f] if it is not bound, and it
returns a non-@racket[#f] value if the identifier is bound.

Change @racket[pfsh] to treat a parenthesized form as a shell command
only if the function position is an unbound identifier, so the
following program works to list all files twice:

@racketmod[
@#,racket[pfsh]

(define (list-all)
  (ls -l))

(list-all)
(list-all)
]}

@item{To make @racket[pfsh] more like other shell languages, it would
be nice if

@racketblock[
ls -l
]

with no parentheses would list all files. One way to make that work is
to say that parentheses are implicit when a source line has multiple
forms all on the same line, without any parentheses around the source
line. With that rule, then

@racketblock[
whoami
ls -l
(whoami)
(ls -l)
]

would work to run @tt{whoami} twice and list files twice (interleaved).

We could implement this rule by writing a character-level parser, but
it turns out that syntax objects from the default S-expression have
enough information to implement implicit parentheses.

Change @racket[pfsh] to allow implicit parentheses by changing
@racket[#%module-begin] to add them. You can determine when syntax
objects are on the same line by using the @racket[syntax-line]
function. You can infer that parentheses are present when
@racket[syntax-e] produces a value for which @racket[pair?] produces a
true value.

Changing @racket[#%module-begin] isn't really the right idea, as we
explore in the next exercise, but try this bad idea, first.}


@item{

The problem with adding implicit parentheses in
@racket[#%module-begin] is that it confuses two layers: The existence
of tokens on the same line is properly a reader-level decision, since
it's about sequences of characters.

As an illustration of the problem, consider this program:
@;
@racketmod[
racket/base
(require (for-syntax syntax/parse
                     syntax/strip-context))

(define-syntax (main-submodule stx)
  (syntax-parse stx
    [(_ word)
     #:with word (strip-context #'word)
     #'(module main pfsh
         echo word)]))

(main-submodule hello)
]
@;
There's a little subtlety here in making @racket[hello] have the right
binding by using @racket[strip-context], but making it have the right
source location to line up with @racket[echo] would be much more
trouble.

The @racketmodname[racket/base] language is not set up for implicit
parentheses, so a better and more consistent strategy for
@racket[pfsh] is to make implicit parentheses part of the reader. Move
your strategy in the previous exercise from @racket[#%module-begin] to
the reader. You can use @racket[#:wrapper1] in
@racketmodname[syntax/module-reader] to adjust the result that the
reader would otherwise return.}


]

