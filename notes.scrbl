#lang scribble/manual
@(require (for-label racket/base
                     racket/class
                     (only-in typed/racket/base :)
                     memoize))

@title{Building Languages with Racket}

@table-of-contents[]

@include-section["part1.scrbl"]
@include-section["part2.scrbl"]
