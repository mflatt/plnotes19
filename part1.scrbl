#lang scribble/manual

@title[#:style '(grouper toc) #:tag "part1"]{Introduction and Macro Foundations}

@local-table-of-contents[]

@include-section["example-langs.scrbl"]
@include-section["syntactic-abstraction.scrbl"]
@include-section["model.scrbl"]
