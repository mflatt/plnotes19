#lang scribble/manual
@(require (for-label racket/base
                     (only-in syntax/parse define-syntax-class)))

@title[#:tag "modules"]{Modules}

We have used predefined @defterm{modules} like @racket[racket/base]
and @racket[syntax/parse]. Also, every Racket program that starts
@hash-lang[] is a @defterm{module}. But we have not yet written
programs that have multiple of our own modules.

@; ----------------------------------------
@section[#:tag "def-modules"]{Defining Modules}

Let's move our @racket[run-program] function into its own module.
While we're at it, let change the name to just @racket[run]. The
@racket[run] function expects strings, so

@racketblock[(run "ls" "-l")]

lists the content of the current directory in long format.

Here's the module:

@filebox[@elemtag["run.rkt"]{@filepath{run.rkt}}
@racketmod[
racket/base
(require racket/system)

(provide run)

(define (run prog . args)
  (apply system* (find-program prog) args))

(define (find-program str)
  (or (find-executable-path str)
      (error 'pfsh "could not find program: ~a" str)))
]]

@;{
@margin-note{It would be better to write
             @racketblock[(provide
                           (contract-out
                            [run (->* (string?)
                                      #:rest (listof string?)
                                 boolean?)]))]
             to ensure that callers provide strings for @racket[prog] and @racket[args].}
}

This module defines @racket[run] and uses the @racket[provide] form to
export it for use by other modules. The module also defines a
@racket[find-program] helper function, but that function is not
exported for external use.

To use @racket[run], another module imports it with @racket[require].
Assuming that @filepath{use-run.rkt} is in the same directory, we can
reference the @filepath{run.rkt} module using a relative path:

@margin-note{Windows users: try @racket[(run "cmd.exe" "/c" "dir")], instead.}

@filebox["use-run.rkt"
@racketmod[
racket
(require "run.rkt")

(run "ls" "-l")
]]

@; ----------------------------------------
@section{Macros and Modules}

With the @racket[run] function in its own module @filepath{run.rkt},
we can implement the @racket[run] macro in a separate module. Using
the same name for a function and a macro is not usually a good idea,
but for example purposes, it illustrates the way that layers of
languages sometimes need to implement one construct in terms of a
lower-level construct that has the same name.

We do have to pick a different module name, though, so let's put the
@racket[run] macro in @filepath{pfsh-run.rkt} (where ``pfsh'' is short
for ``parenthesis-friendly shell'' and is pronounced the same as
``fish''):

@filebox["pfsh-run.rkt"
@racketmod[
racket/base
(require "run.rkt"
         (for-syntax racket/base
                     syntax/parse))

(provide (rename-out [pfsh:run run]))

(define-syntax (pfsh:run stx)
  (syntax-parse stx
    [(_ prog:id arg:id ...)
     #'(run (symbol->string 'prog) (symbol->string 'arg) ...)]))
]]

@filebox["use-pfsh-run.rkt"
@racketmod[
racket/base
(require "pfsh-run.rkt")

(run ls -l)
]]

Note how @filepath{pfsh-run.rkt} defines @racket[pfsh:run] but renames
it to @racket[run] when exporting via @racket[provide]. That way, the
implementation of the macro can refer to the @racket[run] function
imported from @filepath{run.rkt}. The macro system manages bindings
properly to ensure that @racket[run] in the expansion of
@racket[pfsh:run] will always refer to the @racket[run] function, even
if @racket[pfsh:run] is used in a module like
@filepath{use-pfsh-run.rkt} where @racket[run] does not refer to the
function.

@; ----------------------------------------
@section[#:tag "macros-exercises"]{Exercises}

@itemlist[
#:style 'ordered

@item{Put your @racket[define-five] macro in a module that exports it
as just @racketidfont{define}. When you @racket[require] that module
into a @racket[@#,hash-lang[] @#,racketmodname[racket/base]] module,
the @racket[require]d @racketidfont{define} shadows the initial
@racket[define] binding.}

@item{Adjust your @racket[define-five] macro so that @racket[(@#,racketidfont{define}
_id _expr)] defines @racket[_id] as a constant @racket[5] and
@racket[(@#,racketidfont{define} (_id) _expr)] defines @racket[_id] as a function that
returns five, but no other form is allowed (e.g., a function that
takes some arguments is not allowed.}

@item{The most obvious solution to the preceding exercise involves two
different pattern clauses, but with essentially the same template. Try
using @racket[define-syntax-class] (consult the documentation) to
avoid that redundancy. Note that @racket[define-syntax-class] usually
goes inside @racket[begin-for-syntax]:

@racketblock[
(define-for-syntax
 (define-syntax-class
   ....))]}
   
]
