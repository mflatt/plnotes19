#lang scribble/manual
@(require (for-label racket/base)
          scribble/base
          scribble/core
          "little.rkt")

@title[#:tag "syntax-objects"]{Syntax Objects}

Traditional Lisp macros work on S-expressions, but Racket and other
languages in the Scheme family rely on a richer representation of
programs. A @defterm{syntax object} is like an S-expression, but nodes
in a syntax object have source-location and binding information. This
extra information brings back a Plait-like requirement to coerce
between syntax objects and symbols, lists, numbers, etc.

@section{The Little Syntaxer}

@dialog[

@left{@elem[#:style (style 'no-break '())]{What's the value of @racket[1]?@hspace[3]}}

@right{That's the number @racket[1].}

@left{Can we type @racket[1] in DrRacket's interactions window and see the value?}

@right{Sure, assuming that the definitions window starts @racket[@#,hash-lang[] racket].

       @little[1]}

@left{What's the value of @racket[#'1]?}

@right{That's a syntax object that has a @racket[1] inside it.

       @little[#'1]}

@right{I notice that the printed form of a syntax object seems to start with a source
       location, and the line number gets bigger each time we enter a new expression.

       @little[#'1
               #'1]}

@left{Yes it does. Try syntax-quoting some things other than just @racket[1].}

@right{@little[#'2
               #'(+ 3 4)
               #'my-function
               #'(define (f x)
                   (+ x 1))
               #'#t]}

@left{Is there a way to get the @racket[1] out of @racket[#'1]?}

@right{I could print @racket[#'1] and then try to parse the @racket[1] back out.
       That's probably not the right way.}

@left{Try @racket[syntax-e].}

@right{@little[(syntax-e #'1)
               (syntax-e #'2)
               (syntax-e #'(+ 3 4))]

       So, if I want to inspect a big syntax tree, I could use @racket[syntax-e]
       plus list-manipulation functions to look at subtress.}

@left{You could, but @racket[syntax->list] is often easier to use.

      If you want to recursively apply @racket[syntax-e], you can use
      @racket[syntax->datum].}

@right{@little[(syntax->list #'(+ 3 4))
               (syntax->list #'(+ 3 (* 2 2)))
               (syntax->datum #'(+ 3 (* 2 2)))

               (eval:error (datum->syntax '(+ 3 4)))]

       Looks like my guess about @racket[datum->syntax] was wrong.}

@left{A syntax object has more information than an S-expression, and
      you have to supply the extra information. The first argument to
      @racket[datum->syntax] is an existing syntax object whose
      binding information can be copied over, or you can use
      @racket[#f] to mean ``no scopes.''}

@right{@little[(datum->syntax #f '(+ 3 4))
               (datum->syntax #'here '(+ 3 4))]
       I don't see the difference between those two.}

@left{The difference will matter later. For now, take my word for it
      that @racket[#'here] is usually a better choice than @racket[#f].

      @hspace[1]

      What happens if the datum given to @racket[datum->syntax]
      already has syntax objects somewhere inside?}

@right{Maybe they're left as-is.

       @little[(datum->syntax #'here (list '+ #'3 #'4))
               (syntax->list (datum->syntax #'here (list '+ #'3 #'4)))]

       Yes, by pulling the syntax object from @racket[datum->syntax]
       back apart, I can see that the source locations for @racket[#'3]
       and @racket[#'4] where preserved by @racket[datum->syntax].}

@left{What's the value of @racket[`(1 2 3)]?}

@right{Using @litchar{`} is the same as @litchar{'}, except that it
       offers the possibility of escaping with @litchar{,}. Since @racket[`(1
       2 3)] has no escape, it's the same as @racket['(1
       2 3)], which is a list of three numbers.}

@left{What's the value of @racket[#`(1 2 3)]?}

@right{I guess that @litchar{#`} is like @litchar{`} in the same way
       that @litchar{#'} is like @litchar{'}, so that's a syntax object
       that encapsulates a list of three syntax objects.

       @little[#`(1 2 3)]

       And maybe it supports escapes as @litchar{#,}...

       @little[#`(1 2 #,(datum->syntax #'here (+ 3 4)))]}

@left{Did you need that @racket[datum->syntax]?}

@right{@little[#`(1 2 #,(+ 3 4))]

       Apparently not! Why not?}

@left{The @litchar{#`} form inserts a @racket[datum->syntax] for each
      @litchar{#,} escape.}

@right{And if the escaped expression produces a syntax object, then
      the automatic @racket[datum->syntax] has no effect. But I can
      see how the default conversion may be handy sometimes.}

@left{By the way, the @litchar{#'} form is a shorthand for
      @racket[syntax], @litchar{#`} is a shorthand for
      @racket[quasisyntax], @litchar{#,} is a shorthand for
      @racket[unsyntax], and @litchar["#,@"] is a shorthand for
      @racket[unsyntax-splicing].}

@right{@little[#`(1 2 #,@(list #'x #'y))]}

]

@section{Datum to Syntax}

The @racket[datum->syntax] function takes up to four arguments:

@itemlist[

 @item{An existing syntax object to supply binding information for the
       new syntax object. There's no way to get just ``binding
       information'' as a value, so that's why @racket[datum->syntax]
       expects a syntax object to provide that information.}

 @item{The datum to encapsulate and convert recursively.}

 @item{Optionally, a source location or an existing syntax object to
       supply a source location. Even though a source location can be
       provided as a standalone value, it's common to provide another
       syntax object to make the new syntax object have the same
       location as a previous one.

       @little[(define stx #'orig)
               stx
               (datum->syntax #'here 'new stx)
               (datum->syntax #'here 'new #'here)]}

 @item{Optionally, an existing syntax object to supply additional
       properties. Extra properties can record, for example, whether
       the source term used @litchar{()}, @litchar{[]}, or
       @litchar{{}} to group subterms.}

]

