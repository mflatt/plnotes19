#lang scribble/manual
@(require (for-label racket/base
                     ffi/unsafe
                     syntax/parse/define))

@title[#:tag "syntactic-abstraction"]{From Syntactic Abstraction to Language Construction}

The examples in the previous section (@secref["example-langs"])
demonstrate several languages that are relatively distant from the
more primitive @racketmodname[racket/base] language. The point of the
examples was to illustrate Racket's reach. In practice, however, many
programming problems require a much more modest variation of an
existing language than building an entirely new language.

Racket's support for language-oriented programming provides a smooth
path from simple syntactic abstractions (to avoid boilerplate, even
within a single module) to syntactic extension (to provide new
language constructs, such as classes and objects) to full new
languages (such as @racketmodname[typed/racket] or Datalog). The key
parts of Racket that enable that spectrum are

@itemlist[

 @item{@defterm{syntax objects} as a semi-structured, binding-aware
        representation of program fragments;}

 @item{@defterm{macro expansion} to trigger compile-time computations
       that manipulate syntax objects, transforming source programs to
       more primitive terms; and}

 @item{@defterm{modules} for managing bindings so that they can bridge
       compile-time transformations and run-time support in a
       convenient and scalable way.}

]

We sometimes use the word @defterm{macros} as a shorthand for this
combination of technologies in Racket, because writing macro
transformations is often the central task in implementing a language
with Racket.

@; ----------------------------------------
@section{Syntactic Abstraction}

Suppose that you find yourself writing repetitive code when setting up
a bridge between Racket and a C-implemented library using Racket's
FFI:

@racketblock[
(define insstr (get-ffi-obj 'insstr ncurses-lib (_fun #:lock-name "ncurses"
                                                      _string -> _int)))
(define insnstr (get-ffi-obj 'insnstr ncurses-lib (_fun #:lock-name "ncurses"
                                                        _string _int -> _int)))
(define winsstr (get-ffi-obj 'winsstr ncurses-lib (_fun #:lock-name "ncurses"
                                                        _pointer _string -> _int)))
(define winsnstr (get-ffi-obj 'winsnstr ncurses-lib (_fun #:lock-name "ncurses"
                                                          _pointer  _string _int -> _int)))
....
]

Functional abstraction is generally the best way to avoid repetition.
In this case, however, we need to abstract over definitions and
syntactic patterns of @racket[_fun].

This is repetition is easily avoided through a pattern-based macro,
because a macro can generate a definition as easily as an expression.


@racketblock[
(define-simple-macro (defncurses name arg-type ... -> result-type)
  (define name (get-ffi-obj ncurses-lib 'name (_fun #:lock ncurses-lock
                                                    arg-type ... -> result-type))))

(define-ncurses insstr _string -> _int)
(define-ncurses insnstr _string _int -> _int)
(define-ncurses winsstr _pointer _string -> _int)
(define-ncurses winsnstr _pointer  _string _int -> _int)
....
]

Note that the four-dot @racket[....] is intended to represent code not
shown here, but the three-dot @racket[...] in
@racket[define-simple-macro] is part of the syntax of pattern-based
macros. If pattern-based macros seem a little mysterious right now,
that's ok; in a little while, we'll break things down to a more
primitive level, and then we'll build back up to patterns and
templates.

This use of macros to avoid keystrokes is relatively shallow. The
@racket[define-ncurses] form is unlikely to be useful in other
programs, so it is unlikely to be separately documented, and (as we
will see) it is missing some integrity checks that would be needed to
guard against misuse, such as insisting that @racket[name] is matched
to an identifier as opposed to a number. This macro illustrates
@defterm{syntactic abstraction}, but it's @defterm{abstraction} only
in putting common code in one place. A reader of the module that uses
@racket[define-ncurses] is probably going to just look at its
definition to understand what it means (so, not abstract in that
sense).

@; ----------------------------------------
@section{Language Extension}

Another task that may lead to repetitive code is in measuring the time
for an expression to evaluate. That measurement can be implemented in
Racket using @racket[current-inexact-milliseconds]:

@racketblock[
(let* ([before (current-inexact-milliseconds)]
       [answer (fib 30)]
       [after (current-inexact-milliseconds)])
 (printf "It took ~a ms to compute.\n" (- after before))
 answer)
]

Clearly, there's a lot of boilerplate here around the expression to
measure, which is just @racket[(fib 30)]. In this case, we can
abstract the boilerplate into a function:

@racketblock[
(define (time-thunk thunk)
  (let* ([before (current-inexact-milliseconds)]
         [answer (thunk)]
         [after (current-inexact-milliseconds)])
   (printf "It took ~a ms to compute.\n" (- after before))
   answer))

(time-thunk (lambda () (fib 30)))
]

This will be better if we have many expressions to time, since we can
wrap just @racket[(time-thunk (lambda () ....))] around each
expression. Still, it's not great as an abstraction of
@defterm{timing}, because nothing about the problem of timing an
expression says that it should involve thunks. We can create a better
abstraction by using a macro to create the thunk:

@racketblock[
(define-simple-macro (time-it expr)
  (time-thunk (lambda () expr)))

(time-it (fib 30))
]

The @racket[time-it] syntactic form is a good enough abstraction to
put into a library somewhere. (Actually, a form like this already
exists in @racket[racket/base] as @racket[time], but this is how
@racket[time] is implemented.)

When you decide that a function belongs in a library, then you simply
take its implementation along with any private helper function, put it
in the new library, export the function from the new library, and then
import the function in other libraries to use it. The same strategy
works with Racket macros, and that's part of the smooth path from
simple cases to more ambitious cases. That is, the library to provide
@racket[time-it] contains the @racket[time-it] macro and
@racket[time-thunk] function:

@racketmod[
racket/base
(require syntax/parse/define) (code:comment "for define-simple-macro")

(provide time-it)

(define-simple-macro (time-it expr)
  (time-thunk (lambda () expr)))

(define (time-thunk thunk)
  (let* ([before (current-inexact-milliseconds)]
         [answer (thunk)]
         [after (current-inexact-milliseconds)])
   (printf "It took ~a ms to compute.\n" (- after before))
   answer))
]

Only @racket[time-it] is exported. When @racket[time-it] is used in
another module, its expansion introduces a call to
@racket[time-thunk]. The macro system makes that work right, even
though @racket[time-thunk] is not directly available in a module that
uses @racket[time-it].

The @racket[time-it] macro is an example of @defterm{language
extension}. A module in the @racketmodname[racket/base] language that
imports @racket[time-it] gets an extension to the language that
behaves as much a part of the language as any built-in form.

@; ----------------------------------------
@section{Language Construction}

A module that starts @racket[@#,hash-lang[] _X] refers to another
module @racket[_X] in much thte same way that @racket[(require _X)]
refers to a module @racket[_X]. The difference is that
@racket[@#,hash-lang[] _X] gives @racket[_X] the chance to determine
bindings from scratch, instead of strictly adding to the current of
bindings as @racket[(require _X)] does. Since a module can re-provide
bindings from another module, @racket[_X] can extend
@racketmodname[racket/base] or any other module, if it chooses.
Alternatively, a language @racket[_X] can withold @racket[require] so
that it has complete control over bindings in a module that uses
@racket[@#,hash-lang[] _X] .

There's more to the story for languages, including the protocol that
gives @racket[_X] control over character-level parsing. But the idea
that a language is just another module is the key to turning
language extension into language construction.
