#lang racket/base
(require syntax/parse/define
         (for-syntax racket/base))

(define (fib n)
  (cond
    [(= n 0) 0]
    [(= n 1) 1]
    [else (+ (fib (- n 1)) (fib (- n 2)))]))

(define (time-thunk thunk)
  (let* ([before (current-inexact-milliseconds)]
         [answer (thunk)]
         [after (current-inexact-milliseconds)])
    (printf "It took ~a ms to compute.\n" (- after before))
    answer))

(define-syntax (time-it stx)
  #`(time-thunk (lambda () #,(list-ref (syntax->list stx) 1))))

#;
(define-simple-macro (time-it expr)
  (time-thunk (lambda () expr)))

(time-it (fib 30)) ; (time-thunk (lambda () (fib 30)))
(time-it (fib 31))
