#lang scribble/manual
@(require (for-label racket/base))
 
@title{Example Racket Languages}
 
Every Racket program starts with @hash-lang[] and the name of a
language. The language determines the way the rest of the file is
interpreted.
 
Here's an example Racket program that prints @racketresultfont{832040}
when run:
 
@codeblock{
  #lang racket
 
  (define (fib n)
    (cond
      [(= n 0) 0]
      [(= n 1) 1]
      [else (+ (fib (- n 1)) (fib (- n 2)))]))
 
  (fib 30)
}