#lang scribble/manual
@(require (for-label racket/base
                     racket/class
                     (only-in typed/racket/base :)
                     memoize))

@title[#:tag "example-langs"]{Example Racket Languages}

Every Racket program starts with @hash-lang[] and the name of a
language. The language determines the way the rest of the file is
interpreted.

@; ----------------------------------------
@section{Racket Variants}

Here's an example Racket program that prints @racketresultfont{832040}
when run:

@racketmod[
racket

(define (fib n)
  (cond
    [(= n 0) 0]
    [(= n 1) 1]
    [else (+ (fib (- n 1)) (fib (- n 2)))]))

(fib 30)
]

Here's essentially the same program, where the only difference is that
it starts with @racket[@#,hash-lang[] @#,racketmodname[racket/base]]
instead of @racket[@#,hash-lang[] @#,racketmodname[racket]]. The
@racketmodname[racket/base] language starts with fewer functions and
syntactic forms than @racketmodname[racket], but it has everything
that @racket[fib] needs:

@racketmod[
racket/base

(define (fib n)
  (cond
    [(= n 0) 0]
    [(= n 1) 1]
    [else (+ (fib (- n 1)) (fib (- n 2)))]))

(fib 30)
]

As you may expect, the @racketmodname[racket] language is defined by
starting with the @racketmodname[racket/base] language, and
@racketmodname[racket] just adds even more syntactic forms and
predefined functions. A language can start with
@racketmodname[racket/base] and @emph{subtract} bindings and
capabilities just as easily as it can add them.

For example, the following variant of the @racket[fib] example uses
@racket[typed/racket/base] and adds a type declaration for
@racket[fib]. The program runs the same and produces the same result,
but if you change @racket[30] to @racket["30"], then instead of a
run-time error from @racket[=], the program won't even run due to a
static type error. In that sense, @racketmodname[typed/racket/base]
has removed some capabilities from @racketmodname[racket/base] (as
well as adding the additional syntax for @racket[:] and the additional
capability of guaranteeing something about the program's run-time
behavior):

@racketmod[
typed/racket/base

(: fib (Number -> Number))
(define (fib n)
  (cond
    [(= n 0) 0]
    [(= n 1) 1]
    [else (+ (fib (- n 1)) (fib (- n 2)))]))

(fib 30)
]

Here's a variant of @racket[fib] that uses the
@racketmodname[racket/class] library's @racket[class] and @racket[new]
to implement @racket[fib] in a gratuitously class-oriented way. It
uses @racket[require] to add the syntactic forms of
@racketmodname[racket/class] to @racket[racket/base]---which is
possibly only because @racket[racket/base] defined
@racket[require]---in the same way that @racket[require] can be used
to import a library's functions:

@racketmod[
racket/base
(require racket/class)

(define fibber%
  (class object%
    (init-field n)
    (define/public (get-value)
      (cond
        [(= n 0) 0]
        [(= n 1) 1]
        [else (+ (send (new fibber% [n (- n 1)]) get-value)
                 (send (new fibber% [n (- n 2)]) get-value))]))
    (super-new)))

(send (new fibber% [n 30]) get-value)
]

More usefully, here's a variant that uses @racket[define/memo] from
@racket[memoize] to implement a linear-time variant of @racket[fib]:

@racketmod[
racket/base
(require memoize)

(define/memo (fib n)
  (cond
    [(= n 0) 0]
    [(= n 1) 1]
    [else (+ (fib (- n 1)) (fib (- n 2)))]))

(fib 30)
]

@; ----------------------------------------
@section{Changing the Surface Syntax}

The preceding examples illustrate how Racket libraries can provide the
same kinds of things as languages, including both syntactic forms and
functions. A language has one extra capability, however, which is to
define character-level syntax, as illustrated by this Racket program:

@codeblock|{
#lang honu

function fib(n) {
  if (n == 0)
    0
  else if (n == 1)
    1
  else
    fib(n-1) + fib(n-2)
}

fib(30)
}|

Ultimately, @racketmodname[honu] builds on
@racketmodname[racket/base], but @racketmodname[honu] takes more
direct control over the way that programs are parsed. It's easy to
see how a @racketmodname[honu] program can be translated to a
@racketmodname[racket/base] program, that is indeed how
@racketmodname[honu] works.

@; ----------------------------------------
@section{Changing the Evaluation Model}

The following example does not compile to @racketmodname[racket/base]
in such an obvious way, because the evaluation of a Datalog program is
completely different than the evaluation of a Racket program. Still,
the @racket[+] and @racket[-] operations of
@racketmodname[racket/base] are used as ``external queries'' in this
example:

@codeblock|{
#lang datalog
(racket/base).

fib(0, 0).
fib(1, 1).
fib(N, F) :- N != 1,
             N != 0,
             N1 :- -(N, 1),
             N2 :- -(N, 2),
             fib(N1, F1),
             fib(N2, F2),
             F :- +(F1, F2).

fib(30, F)?
}|

The compilation of this module builds up suitable data structures and
generates calls to run-time support functions. Those support functions
implement Datalog's evaluation model, and they are part of the
@racketmodname[datalog] language just as much as its parser. At the
same time, the underlying Racket module and binding machinery makes it
easy for Datalog programs to refer to bindings from other Racket
modules like @racketmodname[racket/base]---even bindings that are
implemented in other Racket languages.

Finally, the following program does not produce the value
@racketresultfont{832040}. Instead, it produces a representation of a
document that starts basically the same as the of the document that
you're currently reading. In @racketmodname[scribble/manual], the
default mode is literal text, and a @litchar["@"] character plays a
role similar to @litchar{\} in LaTeX (except that @litchar["@"] in
Scribble escapes to a real programming language).

@codeblock|{
#lang scribble/manual
@(require (for-label racket/base))

@title{Example Racket Languages}

Every Racket program starts with @hash-lang[] and the name of a
language. The language determines the way the rest of the file is
interpreted.

Here's an example Racket program that prints @racketresultfont{832040}
when run:

@codeblock{
  #lang racket

  (define (fib n)
    (cond
      [(= n 0) 0]
      [(= n 1) 1]
      [else (+ (fib (- n 1)) (fib (- n 2)))]))

  (fib 30)
}
}|

Here, again, the evaluation model for @racketmodname[scribble/manual]
is significantly different than the evaluation of
@racketmodname[racket/base]. More than @racketmodname[datalog],
however, @racketmodname[scribble/manual] takes advantage of an easy
escape to Racket's evaluation model.
